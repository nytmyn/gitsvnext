import subprocess

try:
    from subprocess import check_output as call_and_capture
except ImportError:
    def call_and_capture(*args, **kwargs):
        p = subprocess.Popen(*args, stdout=subprocess.PIPE, **kwargs)
        stdout, _ = p.communicate()
        
        if p.returncode != 0:
            raise subprocess.CalledProcessError(p.returncode, ' '.join(*args))
        return stdout
        
def join_url(url, *paths):
    '''Joins url with several paths
    '''
    return '/'.join([url] + list(paths))
    