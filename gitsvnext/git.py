import os
import re
import subprocess

from mcommand import Abort

class CommandError(Abort):
    pass

from .util import call_and_capture

class Repo(object):
    '''Git repo class
    '''
    def __init__(self, repo_root, bare = False):
        self.repo_root = repo_root
        self.git_dir = os.path.join(repo_root, '.git') if not bare else repo_root
        self.__svn_info = None
        self.__config = {}
        
    def command(self, *args, **opts):
        cmd = ['git']
        cmd += args
        for k, v in opts.iteritems():
            arg = '--' + k if len(k) > 1 else '-' + k
            if not isinstance(v, bool):
                arg += '=' + str(v)
            cmd.append(arg)
        
        try:
            return call_and_capture(cmd, cwd=self.repo_root).strip()
        except OSError:
            raise Abort('failed to execute {0}.'.format(cmd[0]))
        except subprocess.CalledProcessError:
            raise CommandError('git command failed.')
        
    def get_config(self, key, cache=True):
        if cache and key in self.__config:
            val = self.__config[key]
        else:
            try:
                val = self.command('config', key).strip()
            except CommandError:
                val = None
            if cache:
                self.__config[key] = val
        return val
        
    def get_config_list(self, key, cache=True):
        if cache and key in self.__config:
            vals = self.__config[key]
        else:
            try:
                vals = self.command('config', '--get-all', key).strip()
            except CommandError:
                vals = None
            if not vals:
                vals = ()
            else:
                vals = [v.strip() for v in vals.splitlines(False)]
            if cache:
                self.__config[key] = vals
        return vals
        
    def diff(self, *revs):
        return self.command('diff', '--no-color', *revs)
    
    def svn(self, *args, **opts):
        return self.command('svn', *args, **opts)
        
    def svn_info(self):
        if self.__svn_info == None:
            out = self.command('svn', 'info').strip()
            vals = dict()
            for line in (l.strip() for l in out.split('\n')):
                if not ':' in line: continue
                k, v = line.split(':', 1)
                vals[k.strip()] = v.strip()
            
            self.__svn_info = vals
            
        return self.__svn_info
    
    def get_svn_remotes(self, base=None):
        if not base: base = os.path.join(self.git_dir, 'svn/refs/remotes')
        
        def is_remote(name):
            for item in os.listdir(os.path.join(self.git_dir, base, name)):
                if item.startswith('.rev_map.'):
                    return True
        
        remotes = []
        bases = []
        for r in os.listdir(base):
            if is_remote(r):
                remotes.append(r)
            else:
                bases.append(r)
        
        for b in bases:
            names = self.get_svn_remotes(os.path.join(base, b))
            remotes += ['{0}/{1}'.format(b, n) for n in names]
        
        return remotes
    
    def get_active_branch(self):
        branches = self.command('branch')
        for b in branches.splitlines():
            if b.startswith('*'):
                return b[2:].strip()
        return None
        
    def get_upstream_for(self, branch):
        branches = self.command('branch', '-vv', abbrev=7)
        
        for b in branches.splitlines():
            r = re.match(r'^[*\s][\s](.*)\s+[a-f0-9]{7}\s(.*)$', b)
            if not r: continue
            
            name = r.group(1).strip()
            if name == branch:
                r = re.match(r'\[(.*): (?:behind|ahead) [0-9]+\]', r.group(2))
                if r:
                    upstream = r.group(1)
                    return upstream
                break
        return None   
        
def read_svn_externals(file):
    '''reads externals as output byt git svn show-externals from a file-like object
    '''
    parent = None
    externals = []
    for line in file:
        line = line.strip()
        if line.startswith('#'):
            if parent and externals:
                # finish current folder
                yield parent, externals
            parent = line[1:].strip()
            externals = []
        elif line and parent:
            ext = line[len(parent):].strip()
            if ext:
                externals.append(ext)
    
    if parent and externals:
        # finish current folder
        yield parent, externals
        
def load_repo(repopath=None):
    if not repopath:
        lastpath = None
        testpath = os.path.normpath(os.getcwd())
        while testpath != lastpath:
            lastpath = testpath
            gitpath = os.path.join(testpath, '.git')
            if os.path.isdir(gitpath):
                repopath = testpath
                break
            testpath = os.path.dirname(testpath)
            
    if not repopath:
        raise Abort('Not in GIT repository. Use --repo to specify path to it')
    
    return Repo(repopath)