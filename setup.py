#!/usr/bin/env python

import os
try:
    from setuptools import setup
except ImportError:
    print 'setuptools are required for the setup. Get it at http://pypi.python.org/pypi/setuptools...'
    exit(1)

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "gitsvnext",
    version = "0.3",
    install_requires = read('requirements.txt'),
    entry_points = {        
        'console_scripts' : [
            'gitsvnext = gitsvnext:main',
            ]
        },
    author = "Jakub Dvorak",
    author_email = "nytmyn@gmail.com",
    description = ("A tool providng few extensions for git svn repositories (externals handling, diff in svn format)"),
    license = "BSD",
    keywords = "git svn",
    url = "https://bitbucket.org/nytmyn/gitsvnext",
    packages = ['gitsvnext'],
    py_modules = [''],
    long_description=read('README'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
